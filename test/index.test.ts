import 'jest';
// https://jestjs.io/docs/api

let sum = (q: number , w: number): number => {
    return q + w
}

describe('First dummy test', () => {
    it('1 + 2 should be 3', async () => {
        expect(sum(1,2)).toBe(3)
    });    
});