
const getInstallationSettings =
    async ({input}) => {
        const settings = {
            data: {
                gitlabProjectId: "",
                gitProjectUrl: ""
            }
        }
        settings.data.gitlabProjectId = await input('Specify Gitlab Project Id')
        settings.data.gitProjectUrl = await input(
            'Specify Gitlab Project Url',
        )

        return settings
    }

module.exports = {getInstallationSettings}
