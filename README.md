# microfrontend-events

## Before work
```
npm run prepare
```
It will install husky pre-commit check

## Installation

```
npm install
```

## Start development
```
npm start
```

## Linting
```
npm lint
```
