/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  verbose: false,
  testEnvironment: 'node',
  testPathIgnorePatterns: ['/node_modules/', '/dist/', '/lib/'],
  moduleDirectories: ['node_modules', 'src'],
  collectCoverage: true,
  coverageReporters: ['lcov', 'cobertura', 'text-summary', 'json-summary'],
  forceExit: true,
  testMatch: ["**/__tests__/**/*.[jt]s?(x)", "**/?(*.)+(spec|test|it).[tj]s?(x)"],
  reporters: [
    "default",
    ["jest-summary-reporter", { "failuresOnly": false }]
  ]
};

// https://jestjs.io/docs/next/configuration
